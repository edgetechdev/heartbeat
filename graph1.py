from flask import Flask,render_template, send_file, make_response, request, Markup,session,abort, url_for, redirect,flash
from flask_mail import Mail, Message
from matplotlib.dates import DateFormatter
import matplotlib.animation as animation
from datetime import datetime, date
from app import create_app

from flask_sqlalchemy import sqlalchemy, SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, logout_user, login_required, UserMixin






import pandas as pd
import os
import pika
import MySQLdb
import pygal
import sys
import csv
import datetime
import matplotlib.dates as mdates

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import json


from flask_login import current_user
db_name  = "guado"
config_name = os.getenv('FLASK_CONFIG')
app = Flask(__name__)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view


app.secret_key = "super secret key"
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql:///{db}'.format(db=db_name)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

class User(UserMixin):
    def __init__(self,id):
        self.id = id


def create_db():
    """ # Execute this first time to create new db in current directory. """
    db.create_all()

#engine = create_engine('mysql://sally:sally@192.168.88.251:3306/guadoOld-5-12-2018', echo=True)

mail=Mail(app)
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'sender@gmail.com'
app.config['MAIL_PASSWORD'] = 'password'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

db = MySQLdb.connect(host='192.168.88.251', user='sally',passwd='sally', db='guadoOld-5-12-2018')


@app.route('/blast', methods=['GET', 'POST'])

def blast():
		cur = db.cursor()
		cur.execute("SELECT m.MONTH, IFNULL(n.TOTAL,0) TOTAL FROM (SELECT 'January' AS MONTH UNION SELECT 'February' AS MONTH UNION SELECT 'March' AS MONTH UNION SELECT 'April' AS MONTH UNION SELECT 'May' AS MONTH UNION SELECT 'June' AS MONTH UNION SELECT 'July' AS MONTH UNION SELECT 'August' AS MONTH UNION SELECT 'September' AS MONTH UNION SELECT 'October' AS MONTH UNION SELECT 'November' AS MONTH UNION SELECT 'December' AS MONTH) m LEFT JOIN (SELECT MONTHNAME(created_at) AS MONTH,COUNT(MONTHNAME(created_at)) AS TOTAL FROM gd_sms_outgoing_blast WHERE created_at BETWEEN '2018-01-01' AND '2018-12-31' GROUP BY MONTHNAME(created_at),MONTH(created_at) ORDER BY MONTH(created_at)) n ON m.MONTH=n.MONTH;")
		result = cur.fetchall()
		result = dict(result)
		print(dict(result))
		jsn = json.dumps(result)
		s = json.loads(jsn)
		month_list=list(s.keys())
				#print(month_list)
		for key, value in s.items():
			items_list = s.items()
			items_list = list(items_list)
			_, res = zip(*items_list)
			tal=list(res)

			month_lst = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December']
			month_list=list(s.keys())
			y = set(month_lst) & set(month_list)
			t = {'January':1, 'February':2, 'March':3, 'April':4, 'May':5, 'June':6, 'July':7,'August':8, 'September':9, 'October':10, 'November':11, 'December':12}
			def t_value(month_lst):
				return t[month_lst.split('/')[0]]
				month_lst.sort(key=t_value)
				print(month_lst)
			try:
				graph = pygal.Line(x_label_rotation=300)
				graph.title = 'Rate of Incoming and Outgoing Sms Per Month.'
				graph.x_labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December']
				graph.add('OutgoingBlast',  tal[0:])
				graph_data = graph.render_data_uri()
				return render_template("base.html", graph_data = graph_data)
			except Exception as e:
				return(str(e))
		return render_template("base.html")
	

			


@app.route('/inout', methods=['GET', 'POST'])
def inout():
	#db = MySQLdb.connect(host='192.168.88.251', user='sally',passwd='sally', db='guadoOld-5-12-2018')
	cur = db.cursor()
	cur.execute("SELECT m.MONTH, IFNULL(n.TOTAL,0) TOTAL FROM (SELECT 'January' AS MONTH UNION SELECT 'February' AS MONTH UNION SELECT 'March' AS MONTH UNION SELECT 'April' AS MONTH UNION SELECT 'May' AS MONTH UNION SELECT 'June' AS MONTH UNION SELECT 'July' AS MONTH UNION SELECT 'August' AS MONTH UNION SELECT 'September' AS MONTH UNION SELECT 'October' AS MONTH UNION SELECT 'November' AS MONTH UNION SELECT 'December' AS MONTH) m LEFT JOIN (SELECT MONTHNAME(created_at) AS MONTH,COUNT(MONTHNAME(created_at)) AS TOTAL FROM gd_sms_outgoing WHERE created_at BETWEEN '2018-01-01' AND '2018-12-31' GROUP BY MONTHNAME(created_at),MONTH(created_at) ORDER BY MONTH(created_at)) n ON m.MONTH=n.MONTH;")
	result1 = cur.fetchall()
	result1 = dict(result1)
	print(dict(result1))
	jsn = json.dumps(result1)
	s = json.loads(jsn)
	month_list=list(s.keys())
	#print(month_list)
	for key, value in s.items():
		items_list = s.items()
		items_list = list(items_list)
		_, res = zip(*items_list)
		inc=list(res)
	cur.execute("SELECT COUNT(id) FROM gd_sms_outgoing_ondemand;")
	sum1=cur.fetchall()
	cur.execute("SELECT COUNT(id) FROM gd_sms_outgoing;")
	sum2=cur.fetchall()
	if sum1 < sum2:
		msg = Message('Hello, Service Provider', sender = 'sllmusanga7@gmail.com', recipients = ['receiver@gmail.com '])
		msg.body = "There's a low number of Incoming SmS's"
		mail.send(msg)
	else:
		print("Everything ok" )



	cur.execute("SELECT m.MONTH, IFNULL(n.TOTAL,0) TOTAL FROM (SELECT 'January' AS MONTH UNION SELECT 'February' AS MONTH UNION SELECT 'March' AS MONTH UNION SELECT 'April' AS MONTH UNION SELECT 'May' AS MONTH UNION SELECT 'June' AS MONTH UNION SELECT 'July' AS MONTH UNION SELECT 'August' AS MONTH UNION SELECT 'September' AS MONTH UNION SELECT 'October' AS MONTH UNION SELECT 'November' AS MONTH UNION SELECT 'December' AS MONTH) m LEFT JOIN (SELECT MONTHNAME(created_at) AS MONTH,COUNT(MONTHNAME(created_at)) AS TOTAL FROM gd_sms_outgoing_ondemand WHERE created_at BETWEEN '2018-01-01' AND '2018-12-31' GROUP BY MONTHNAME(created_at),MONTH(created_at) ORDER BY MONTH(created_at)) n ON m.MONTH=n.MONTH;")
	result2 = cur.fetchall()
	result2 = dict(result2)
	print(dict(result2))
	jsn = json.dumps(result2)
	s = json.loads(jsn)
	month_list=list(s.keys())
	#print(month_list)
	for key, value in s.items():
		items_list = s.items()
		items_list = list(items_list)
		_, res = zip(*items_list)
		out=list(res)

	month_lst = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December']
	month_list=list(s.keys())
	y = set(month_lst) & set(month_list)
	t = {'January':1, 'February':2, 'March':3, 'April':4, 'May':5, 'June':6, 'July':7,'August':8, 'September':9, 'October':10, 'November':11, 'December':12}
	def t_value(month_lst):
		return t[month_lst.split('/')[0]]
		month_lst.sort(key=t_value)
		print(month_lst)
	try:
		graph = pygal.Line(x_label_rotation=300)
		graph.title = 'Rate of OutgoingBlast and OutgoingOndemand Sms Per Month.'
		graph.x_labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December']
		graph.add('OutgoingBlast',  inc[0:])
		graph.add('OutgoingOndemand',  out[0:])
		#graph.add('Outgoing',  sal[0:])
		graph_data = graph.render_data_uri()
		
	
		return render_template("base.html", graph_data = graph_data)
	except Exception as e:
		return(str(e))


	print (out)
	#print(out[0:])
@app.route('/check_interval', methods=['GET','POST'])
def intervals():
	if request.method == 'POST':
		
		#db = MySQLdb.connect(host='192.168.88.251', user='sally',passwd='sally', db='guadoOld-5-12-2018')
		cur = db.cursor()
		#r = cur.execute("SELECT  COUNT(id) FROM gd_sms_outgoing_ondemand where updated_at BETWEEN 's_month' AND 'e_month' ;")
		#r = cur.fetchone()
		#print (s_month)
		s_month = request.form['s_month']
		e_month = request.form['e_month']
		#cur.execute("SELECT SUM(id) FROM gd_sms_outgoing_blast WHERE  STR_TO_DATE(created_at, '%Y/%m/%d') BETWEEN STR_TO_DATE(@s_month, '%Y/%m/%d') AND STR_TO_DATE(@e_month, '%Y/%m/%d') ;")
		
		cur.execute("CREATE PROCEDURE Monthly(@s_month DATE, @e_month DATE) AS BEGIN DECLARE @s_month DATE = 's_month' DECLARE @s_month DATE = 'e_month' SELECT SUM(id) FROM gd_sms_outgoing_blast WHERE created_at  BETWEEN @s_month AND @e_month END;")
		t = cur.fetchall()

		print(t)

		db.commit()

		#return '''<h1>The s_month value is: {}</h1>
                  #<h1>The e_month value is: {}</h1>'''.format(s_month, e_month)

	return '''<form method="POST">
                  Start_Month: <input type="date" name="s_month"min="2018-01-01" max="2018-11-30"><br><br>
                  End_Month: <input type="date" name="e_month"min="2018-02-01" max="2018-12-31"><br><br>
                  <input type="submit" value="Submit"><br>
              </form>'''



@app.route('/login', methods = ['GET', 'POST'])
def login():
   if request.method == 'POST':
      session['username'] = request.form['username']
      return redirect(url_for('blast'))
   return render_template('login.html')






@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('username', None)
   return redirect(url_for('login'))